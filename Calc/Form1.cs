﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc
{
    public partial class Form1 : Form
    {
        double value = 0;
        string operation = "";
        bool operation_pressed = false;

        public Form1()
        {
            InitializeComponent();
        }
       
        private void n_Click(object sender, EventArgs e)
        {
            if ((result.Text == "0")||operation_pressed)
            {
                result.Clear();
            }
            operation_pressed = false;
            Button b = (Button)sender;
            if (b.Text == ",")
            {
                if(!result.Text.Contains(","))
                    result.Text = result.Text + b.Text;
            }
            else
                result.Text = result.Text + b.Text; 
            
        }

        private void buttonce_Click(object sender, EventArgs e)
        {
            result.Text = "0";
        }

        private void opertor_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;

            if (value != 0)
            {
                equal.PerformClick();
                operation_pressed = true;
                operation = b.Text;
                equation.Text = value + " " + operation;
            }
            else
            {
                operation = b.Text;
                value = Double.Parse(result.Text);
                operation_pressed = true;
                equation.Text = value + " " + operation;
            }            
        }

        private void equal_Click(object sender, EventArgs e)
        {
            //operation_pressed = false;
            equation.Text = "";

            switch (operation)
            {
                case "+":
                    result.Text = (value + Double.Parse(result.Text)).ToString();
                    break;
                case "-":
                    result.Text = (value - Double.Parse(result.Text)).ToString();
                    break;
                case "*":
                    result.Text = (value * Double.Parse(result.Text)).ToString();
                    break;
                case "/":
                    if (result.Text == "0")
                    {
                        MessageBox.Show("Cannot divide by zero !");
                    } else
                        result.Text = (value / Double.Parse(result.Text)).ToString();
                    break;
                default:
                    break;
            }

            value = Int32.Parse(result.Text);
            operation = "";
        }

        private void buttonc_Click(object sender, EventArgs e)
        {
            result.Text = "0";
            value = 0;
            equation.Text = "";
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar.ToString())
            {
                case "1":
                    n1.PerformClick();
                    break;
                case "2":
                    n2.PerformClick();
                    break;
                case "3":
                    n3.PerformClick();
                    break;
                case "4":
                    n4.PerformClick();
                    break;
                case "5":
                    n5.PerformClick();
                    break;
                case "6":
                    n6.PerformClick();
                    break;
                case "7":
                    n7.PerformClick();
                    break;
                case "8":
                    n8.PerformClick();
                    break;
                case "9":
                    n9.PerformClick();
                    break;
                case "0":
                    n0.PerformClick();
                    break;
                case "+":
                    add.PerformClick();
                    break;
                case "-":
                    sub.PerformClick();
                    break;
                case "*":
                    mult.PerformClick();
                    break;
                case "/":
                    div.PerformClick();
                    break;
                case ".":
                    point.PerformClick();
                    break;
                case "=":
                    equal.PerformClick();
                    break;
                default:
                    break;
            }
        }
    }
}
